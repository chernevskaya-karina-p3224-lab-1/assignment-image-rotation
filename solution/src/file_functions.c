#include "file_functions.h"
#include "enums.h"
#include "extra_functions.h"
#include <stdio.h>
#include <stdlib.h>

#define BMP_INFO_SIZE sizeof(struct BMPInfo)
#define PIXEL_SIZE sizeof(struct pixel)

//Компилится хорошо, пока нареканий нет
// Функция чтения файла
enum read_status read_BMP_file(const char* input_file_path, struct BMPInfo* info, struct image* image) {
    FILE* file = fopen(input_file_path, "rb");
    if (file == NULL) {
        return READ_INVALID_FILE;
    }

    if (fread(info, BMP_INFO_SIZE, 1, file) != 1) {
        fclose(file);
    return READ_INVALID_HEADER;
    }
    
    image->width = info->width;
    image->height = info->height;
    image->data = malloc(image->height * image->width * PIXEL_SIZE);
    if (image->data == NULL) {
        fclose(file);
        return READ_MEMORY_ALLOCATION_FAILED;
    }

    uint8_t padding = calculate_padding(image->width);

    for (size_t i = 0; i < image->height; i++) {
        if (fread(&(image->data[i * image->width]), PIXEL_SIZE, image->width, file) != image->width) {
            free(image->data);
            fclose(file);
            return READ_INVALID_PIXELS;
        }

        if (fseek(file, (long)padding, SEEK_CUR) != 0) {
            free(image->data);
            fclose(file);
            return READ_FSEEK_ERROR;
        }
}
    fclose(file);
    return READ_OK;
}

//Компилится хорошо, пока нареканий нет
//Функция записи в файл
enum write_status write_data_in_file(const char* output_file_path, struct BMPInfo* info, struct image* image) {
    FILE* output_file = fopen(output_file_path, "wb");
    if (output_file == NULL) {
        return WRITE_FILE_OPEN_ERROR;
    }
    uint8_t padding = calculate_padding(image->width);
    if (!fwrite(info, BMP_INFO_SIZE, 1, output_file)) {
        fclose(output_file);
        return WRITE_INFO_WRITE_ERROR;
    }
    

    for (size_t j = 0; j < image->height; j++) {
        if (!fwrite(&image->data[j * image->width], image->width * PIXEL_SIZE, 1, output_file)) {
            fclose(output_file);
            return WRITE_PIXELS_WRITE_ERROR;
        }

        for(size_t i = 0; i < padding; i++){
            if (fputc(0, output_file) == EOF) {
                fclose(output_file);
                return WRITE_PADDING_WRITE_ERROR; 
            }
        }
    }

    fclose(output_file);
    return WRITE_OK;
}
