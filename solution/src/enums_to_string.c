#include "enums.h"

const char* write_status_to_string(enum write_status status) {
    switch (status)
    {
    case WRITE_INFO_WRITE_ERROR:
        return "Error writing to header";
    case WRITE_PIXELS_WRITE_ERROR:
        return "Error writing a raster array";
    case WRITE_PADDING_WRITE_ERROR:
        return "FPUTC error";
    default:
        return "Unknown Status";
    }
}

const char* read_status_to_string(enum read_status status) {
    switch (status) {
        case READ_INVALID_FILE:
            return "Invalid file. Make sure that the file is in the same directory as the program and also has the BMP extension.";
        case READ_INVALID_BITS:
            return "Bit reading error";
        case READ_INVALID_HEADER:
            return "Error reading the file header";
        case READ_MEMORY_ALLOCATION_FAILED:
            return "The memory allocation for image data failed during the read operation";
        case READ_INVALID_PIXELS:
            return "Pixels reading error";
        case READ_FSEEK_ERROR:
            return "FSEEK error";
        default:
            return "Unknown Status";
    }
}
