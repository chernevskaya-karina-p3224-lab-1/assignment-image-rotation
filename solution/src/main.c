#include <stdio.h>
#include <stdlib.h>

#include "extra_functions.h"
#include "file_functions.h"
#include "structs.h"


int main(int argCount, char* args[]){
    struct BMPInfo info;
    struct image image;
    struct image imageOut;
    struct BMPInfo infoForNew;
    if (argCount!=3)
    {
        return EXIT_FAILURE;
    }
    const char* inputFilePath = args[1];
    const char* outputFilePath = args[2];
    enum read_status read_status;
    enum write_status write_status;

    read_status = read_BMP_file(inputFilePath, &info, &image);
    if (read_status!=READ_OK)
    {
        printf( "%s", read_status_to_string(read_status));
        free(image.data);
        return EXIT_FAILURE;
    }
    

    
    if (rotate(&image, &imageOut, &info, &infoForNew)!=0){
        printf("Error during rotation");
        free(image.data); 
        return EXIT_FAILURE;
    }

    write_status = write_data_in_file(outputFilePath, &infoForNew, &imageOut);
    if (write_status!=WRITE_OK)
    {
        printf( "%s", write_status_to_string(write_status));
        free(imageOut.data);
        return EXIT_FAILURE;
    }
    
    free(image.data);
    free(imageOut.data);
    return EXIT_SUCCESS;
    
}

