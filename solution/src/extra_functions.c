
#include "extra_functions.h"
#include <stdlib.h>

#define ROTATE_SUCCESS 0
#define ROTATE_MEMORY_ERROR 1
//Компилится хорошо, пока нареканий нет(скорее всего, ошибка где-то здесь)
//Функция поворота картинки, принимает заполненный объект первоначальной картинки и пустой для новой картинки
int rotate(struct image* image, struct image* imageOut, struct BMPInfo* info, struct BMPInfo* infoForNew) {
//меняем местами ширину и высоту, создаем в стеке новое пространство для смены пикселей
    imageOut->width = image->height;
    imageOut->height = image->width;
    imageOut->data = (struct pixel*)malloc(imageOut->height * imageOut->width * sizeof(struct pixel));
    if(!imageOut->data) return ROTATE_MEMORY_ERROR;

    for (size_t j = 0; j < imageOut->height; j++) {
        for (size_t i = 0; i < imageOut->width; i++) {
            imageOut->data[j * imageOut->width + i] = image->data[(image->height - i - 1) * image->width + j];
        }
    }
    *infoForNew = *info;
    infoForNew->height = imageOut->height;
    infoForNew->width = imageOut->width;

    return ROTATE_SUCCESS;
}

uint8_t calculate_padding(uint64_t width){
    uint64_t bytes_in_row = width* sizeof(struct pixel);
    return (4 - (bytes_in_row % 4))%4;
}

