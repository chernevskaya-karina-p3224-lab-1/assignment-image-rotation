#ifndef FILE_FUNCTIONS
#define FILE_FUNCTIONS
#include "enums.h"
#include "structs.h"

enum read_status read_BMP_file(const char* input_file_path, struct BMPInfo* info, struct image* image);
enum write_status write_data_in_file(const char* output_file_path, struct BMPInfo* info, struct image* image);

#endif
