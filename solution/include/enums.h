#ifndef ENUMS
#define ENUMS

enum read_status  {
  READ_OK,
  READ_INVALID_FILE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_MEMORY_ALLOCATION_FAILED,
  READ_INVALID_PIXELS,
  READ_FSEEK_ERROR
  };


enum  write_status  {
  WRITE_OK,
  WRITE_INFO_WRITE_ERROR,
  WRITE_PIXELS_WRITE_ERROR,
  WRITE_FILE_OPEN_ERROR,
  WRITE_PADDING_WRITE_ERROR
};

const char* read_status_to_string(enum read_status status);
const char* write_status_to_string(enum write_status status);

#endif


