#ifndef EXTRA_FUNCTIONS
#define EXTRA_FUNCTIONS
#include "structs.h"

int rotate(struct image* image, struct image* imageOut, struct BMPInfo* info, struct BMPInfo* infoForNew);
uint8_t calculate_padding(uint64_t width);

#endif
